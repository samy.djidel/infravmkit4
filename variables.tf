variable "aws_access_key" {
  type = string
  description = "AWS access key"
}
variable "aws_secret_key" {
  type = string
  description = "AWS secret key"
}

variable "linux_instance_name" {
        description = "Name of the instance to be created"
        default = "Linux Server"
}

#variable "windows_instance_name" {
#        description = "Name of the instance to be created"
#        default = "Windows Server"
#}

variable "instance_type" {
        default = "t2.micro"
}

variable "subnet_id" {
        description = "The VPC subnet the instance(s) will be created in"
        default = "subnet-0ff6cd36460bf12e3"
}

variable "ami_linux_id" {
        description = "The AMI to use"
        default = "ami-052f10f1c45aa2155"
}

variable "number_of_instances" {
        description = "number of instances to be created"
        default = 2
}

variable "ssh_key" {
  default = "the_great_aws_key2"
}
